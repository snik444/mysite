﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BetonMashNew.Startup))]
namespace BetonMashNew
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
