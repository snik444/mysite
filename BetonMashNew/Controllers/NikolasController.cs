﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BetonMashNew.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Web.Security;
using Microsoft.AspNet.Identity.Owin;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BetonMashNew.Controllers
{

        [Authorize]
        [Authorize(Users = "Nikolas")]
    public class NikolasController : Controller
    {
        //
        // GET: /Nikolas/
       // [InitializeSimpleMembershipAttribute]

                public NikolasController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

                public NikolasController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        public ActionResult Index()
        {
            List<AdminInfo> a;
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
             //   var a = mb.Users.ToList();
                a = (from u in mb.Users select new AdminInfo() { UserName=u.UserName, KodOPK=u.KodOPK, NameCompany=u.NameCompany, UserId=u.Id, M=u.M, Email=u.Email }).ToList();


            foreach (AdminInfo b in a)
            {
                string SRoles = "";
                foreach (string r in UserManager.GetRoles(b.UserId).ToList())
                    SRoles += r + "; ";
                b.Rol = SRoles;

               string s = "";
                foreach(RegionsAndDealer r in mb.RegionsAndDealers.Where(x=>x.NameUser==b.UserName).ToList())
                {
                    s += mb.Regions.Where(x => x.RegionId == r.RegionId).First().Name+"; ";
                }
               b.Reg = s;
            }
            }
            return View(a);
        }


        public ActionResult Delete(string id = "")
        {
            if (id != User.Identity.Name)
                using (ApplicationDbContext mb = new ApplicationDbContext())
                {                    
                    mb.Users.Remove(mb.Users.Where(x => x.UserName == id).First());
                    mb.RegionsAndDealers.RemoveRange(mb.RegionsAndDealers.Where(x => x.NameUser == id).ToList());
                    mb.SaveChanges();
                }
                return RedirectToAction("Index");
        }

        public ActionResult DelRegion(string id = "", int regId = 0)
        {
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
                mb.RegionsAndDealers.Remove((mb.RegionsAndDealers.Where(x => x.Id == regId)).First());
                mb.SaveChanges();
                List<CountryCl> Countries = mb.Countries.ToList();
                int CountryId = Countries[0].CountryId;
                List<RegionCl> Regions = mb.Regions.Where(x => x.Country == CountryId).ToList();
                ViewBag.Countries = Countries;
                ViewBag.Regions = Regions;
                ViewBag.Name = id;
                return View("Edit", ((from tUR in mb.RegionsAndDealers join tR in mb.Regions on tUR.RegionId equals tR.RegionId where tUR.NameUser == id select new MyRegionCl() { RegionId = tR.RegionId, Name = tR.Name, Country = tR.Country, IdUR = tUR.Id }).ToList()));
   
            }

        }

        public async Task<ActionResult> Role(string id = "", string Rol = "", bool C = false)
        {
            string IdUser="";
            using(ApplicationDbContext mb = new ApplicationDbContext())
            {
                IdUser = mb.Users.Where(x => x.UserName == id).First().Id;
            }
            if(C==true)
                await UserManager.AddToRoleAsync(IdUser, Rol);
            else
                await UserManager.RemoveFromRoleAsync(IdUser, Rol);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(string name="", int Region=0)
        {
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
                List<RegionsAndDealer> R = mb.RegionsAndDealers.Where(x=>x.NameUser==name && x.RegionId==Region).ToList();
                if (R.Count == 0)
                {
                    RegionsAndDealer a = new RegionsAndDealer() { NameUser = name, RegionId = Region };
                    mb.RegionsAndDealers.Add(a);
                    mb.SaveChanges();
                }
                List<CountryCl> Countries = mb.Countries.ToList();
                int CountryId = Countries[0].CountryId;
                List<RegionCl> Regions = mb.Regions.Where(x => x.Country == CountryId).ToList();
                ViewBag.Countries = Countries;
                ViewBag.Regions = Regions;
                ViewBag.Name = name;
                return View((from tUR in mb.RegionsAndDealers join tR in mb.Regions on tUR.RegionId equals tR.RegionId where tUR.NameUser == name select new MyRegionCl() { RegionId = tR.RegionId, Name = tR.Name, Country = tR.Country, IdUR = tUR.Id }).ToList());
            }
        }

        [HttpGet]
        public ActionResult Edit(string id="")
        {
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
                List<CountryCl> Countries = mb.Countries.ToList();
                int CountryId = Countries[0].CountryId;
                List<RegionCl> Regions = mb.Regions.Where(x => x.Country == CountryId).ToList();
                ViewBag.Countries = Countries;
                ViewBag.Regions = Regions;
                ViewBag.Name = id;
                return View((from tUR in mb.RegionsAndDealers join tR in mb.Regions on tUR.RegionId equals tR.RegionId where tUR.NameUser == id select new MyRegionCl() { RegionId = tR.RegionId, Name = tR.Name, Country = tR.Country, IdUR = tUR.Id }).ToList());
            }
        }

        public ActionResult EditM(string id="", bool M=false)
        {
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
               var a= mb.Users.Where(x => x.UserName==id).First();
               a.M = M;
               mb.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<ActionResult> Take(string id, string name)
        {
            List<string> Roles = new List<string>();
            string IdAdd="", IdDel="";
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
                IdAdd = mb.Users.Where(x => x.UserName == id).First().Id;
                IdDel = mb.Users.Where(x => x.UserName == name).First().Id;
                Roles = UserManager.GetRoles(IdDel).ToList();
                foreach(RegionsAndDealer Rad in mb.RegionsAndDealers.Where(x=>x.NameUser==name).ToList())
                {
                    if(mb.RegionsAndDealers.Where(x=>x.NameUser==id && x.RegionId==Rad.RegionId).ToList().Count==0)
                    {
                        mb.RegionsAndDealers.Add(new RegionsAndDealer() { NameUser = id, RegionId=Rad.RegionId });
                    }
                }
                mb.RegionsAndDealers.RemoveRange(mb.RegionsAndDealers.Where(x => x.NameUser == name));
                foreach(Client Clients in mb.Clients.Where(x=>x.Dealer==name).ToList())
                {
                    Clients.Dealer = id;
                }
                mb.SaveChanges();
            }
            foreach (string Rol in Roles)
            {

                    await UserManager.AddToRoleAsync(IdAdd, Rol);
                if (Rol != "User")
                {
                    await UserManager.RemoveFromRoleAsync(IdDel, Rol);
                }
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Take(string id)
        {
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
                ViewBag.Name = id;
                ViewBag.Users = mb.Users.Where(x =>  x.UserName!=id).ToList();
                return View();
            }
        }
	}
}