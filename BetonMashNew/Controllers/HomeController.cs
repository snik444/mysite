﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Web.Security;
using Microsoft.AspNet.Identity.Owin;
using System.Security.Claims;
using System.Threading.Tasks;
using BetonMashNew.Models;

namespace BetonMashNew.Controllers
{
     [Authorize]
    public class HomeController : Controller
    {
        public HomeController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

                        public HomeController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }


        public UserManager<ApplicationUser> UserManager { get; private set; }

        public void LR()
        {
            ViewBag.Roles = UserManager.GetRoles(User.Identity.GetUserId()).ToList();
        }

        public ActionResult Index()
        {
            LR();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
        
        public void VoidMyClients(int reg=0)
        {
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
                try
                {
                    List<RegionCl> Regions = (from RaU in mb.RegionsAndDealers join R in mb.Regions on RaU.RegionId equals R.RegionId where RaU.NameUser == User.Identity.Name select R).ToList();
                    int RegionId;
                    if (reg == 0)
                        RegionId = Regions[0].RegionId;
                    else
                        RegionId = reg;
                    List<CityCl> Cities = mb.Cities.Where(x => x.Region == RegionId).ToList();
                    ViewBag.Regions = Regions;
                    ViewBag.Cities = Cities;
                    List<ClientCi> Clients = (from c in mb.Clients join r in mb.Regions on c.Region equals r.RegionId join city in mb.Cities on c.City equals city.CityId where c.Dealer == User.Identity.Name && c.Ex == true select new ClientCi() { City = c.City, Country = c.Country, Dat = c.Dat, Dealer = c.Dealer, KodOPK = c.KodOPK, NameCity = city.Name, NameRegion = r.Name, NameCompany = c.NameCompany, Note = c.Note, Region = c.Region, UserId = c.UserId }).ToList();
                    if (Clients.Count > 0)
                        ViewBag.Clients = Clients;
                    else
                        ViewBag.Clients = null;
                }
                catch
                {
                    ViewBag.Regions = null;
                    ViewBag.Cities = null;                 
                    ViewBag.Clients = null;
                }
            }
        }

        [HttpPost]
         [Authorize(Roles = "Dealer")]
        public ActionResult MyClients(Client A)
        {
            LR();
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
                if(A!=null)
                {

                        if ((mb.RegionsAndDealers.Where(x => x.RegionId == A.Region && x.NameUser == User.Identity.Name).ToList().Count > 0))
                        {
                            A.Country = mb.Regions.Where(x => x.RegionId == A.Region).First().Country;
                            A.Ex = true;
                            A.Dat = DateTime.Now;
                            A.Dealer = User.Identity.Name;
                            if ((from c in mb.Clients where c.KodOPK == A.KodOPK && c.Country == A.Country && c.Ex == true select c).ToList().Count > 0)
                            {
                                return RedirectToAction("Error", "System", new { Error = " клиент с этим ОКПО уже существует в вашей стране" });
                            }
                            else
                            {
                                mb.Clients.Add(A);
                                mb.SaveChanges();
                            }
                        }
                }
            }
            VoidMyClients();
            return View();


        }

        [HttpGet]
        [Authorize(Roles = "Dealer")]
        public ActionResult MyClients()
        {
            LR();
            VoidMyClients();
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "Dealer")]
        public ActionResult EditClient(int id)
        {
            LR();
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
                Client Cl = (mb.Clients.Where(x => x.UserId == id && x.Dealer == User.Identity.Name).First());
                VoidMyClients(Cl.Region);
                return View(Cl);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Dealer")]
        public ActionResult EditClient(Client A)
        {
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {       
                    var q = (from a in mb.Clients where a.UserId == A.UserId && a.Dealer == User.Identity.Name select a).First();
                    mb.UpdateClients.Add(new UpdateClient() { NameCompany = q.NameCompany, NameDealer = q.Dealer,  Dat = DateTime.Now, Note = "Примечениае "+q.Note+"; Код ОКПО"+q.KodOPK+"; Код города "+q.City });
                    q.Region = A.Region;
                    q.City = A.City;
                    q.Note = A.Note;
                    q.NameCompany = A.NameCompany;
                    q.KodOPK = A.KodOPK;
                if((from c in mb.Clients where c.KodOPK == q.KodOPK && c.Country == q.Country && q.Ex == true && c.UserId!=q.UserId select c).ToList().Count > 0)
                return RedirectToAction("Error", "System", new { Error = " клиент с этим ОКПО уже существует в вашей стране" });

                    mb.SaveChanges();

            }
            return RedirectToAction("MyClients");
        }

        [Authorize(Roles = "Dealer")]
        public ActionResult DeleteClient(int id)
        {
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
                var q = (from a in mb.Clients where a.UserId == id && a.Dealer == User.Identity.Name select a).First();
                q.Ex = false;
                mb.SaveChanges();
            }
            return RedirectToAction("MyClients");
        }

        [Authorize(Roles = "Dealer")]
        public ActionResult AllClients()
        {
            LR();
            VoidMyClients();
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
                //List<RegionCl> Reg = (from R in mb.Regions join RaD in mb.RegionsAndDealers on R.RegionId equals RaD.RegionId where RaD.NameUser==User.Identity.Name select R).ToList();
                //List<Client> Cli = (from c in mb.Clients where c.Dealer!=User.Identity.Name select c).ToList();
                //List<Client> Qc = (from C in Cli join R in Reg on C.Region equals R.RegionId select C).ToList();
                bool M = mb.Users.Where(x => x.UserName == User.Identity.Name).First().M;
                ViewBag.M = M;
                List<RegionCl> Reg = (from R in mb.Regions join RaD in mb.RegionsAndDealers on R.RegionId equals RaD.RegionId where RaD.NameUser == User.Identity.Name select R).ToList();
                List<ClientCi> Clients = new List<ClientCi>();
                foreach (RegionCl FR in Reg)
                {
                    List<ClientCi> Cl = (from c in mb.Clients join City in mb.Cities on c.City equals City.CityId where c.Region == FR.RegionId && c.Ex == true && c.Dealer != User.Identity.Name select new ClientCi() {  City = c.City, Country = c.Country, Dat = c.Dat, Dealer = c.Dealer, KodOPK = c.KodOPK, NameCity = City.Name, NameRegion = FR.Name, NameCompany = c.NameCompany, Note = c.Note, Region = c.Region, UserId = c.UserId } ).ToList();
                    Clients.AddRange(Cl);
                }
                if (Clients.Count > 0)
                      ViewBag.Strangers = Clients;
                else
                    ViewBag.Strangers = null;
            }
            return View();
        }
    
    }
}