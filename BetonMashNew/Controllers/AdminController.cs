﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BetonMashNew.Controllers;
using BetonMashNew.Models;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;


namespace BetonMashNew.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
                public AdminController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

                        public AdminController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

                        public UserManager<ApplicationUser> UserManager { get; private set; }

                        public void LR()
                        {
                            ViewBag.Roles = UserManager.GetRoles(User.Identity.GetUserId()).ToList();
                        }

        public ActionResult Index()
        {
            LR();
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
                List<CountryCl> Countries = mb.Countries.ToList();
                Countries.Insert(0,new CountryCl() { CountryId = 0, Name = "- Выберете страну -" });
                ViewBag.Countries = Countries;

            }
            return View();
        }

        public ActionResult Info(int Country, int Region=0, int City=0)
        {
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
                List<ClientCi> Cl = new List<ClientCi>();
                if (Region == 0 && City == 0)
                    Cl = (from c in mb.Clients join CityP in mb.Cities on c.City equals CityP.CityId join Regs in mb.Regions on c.Region equals Regs.RegionId join Del in mb.Users on c.Dealer equals Del.UserName where c.Ex == true && c.Country == Country select new ClientCi() { City = c.City, Country = c.Country, Dat = c.Dat, Dealer = Del.NameCompany+" ("+Del.KodOPK+")", KodOPK = c.KodOPK, NameCity = CityP.Name, NameRegion = Regs.Name, NameCompany = c.NameCompany+" ("+c.KodOPK+")", Note = c.Note, Region = c.Region, UserId = c.UserId }).ToList();

                if (Region > 0 && City == 0)
                    Cl = (from c in mb.Clients join CityP in mb.Cities on c.City equals CityP.CityId join Regs in mb.Regions on c.Region equals Regs.RegionId join Del in mb.Users on c.Dealer equals Del.UserName where c.Ex == true && c.Country == Country && c.Region == Region select new ClientCi() { City = c.City, Country = c.Country, Dat = c.Dat, Dealer = Del.NameCompany + " (" + Del.KodOPK + ")", KodOPK = c.KodOPK, NameCity = CityP.Name, NameRegion = Regs.Name, NameCompany = c.NameCompany + " (" + c.KodOPK + ")", Note = c.Note, Region = c.Region, UserId = c.UserId }).ToList();

                if (Region > 0 && City > 0)
                    Cl = (from c in mb.Clients join CityP in mb.Cities on c.City equals CityP.CityId join Regs in mb.Regions on c.Region equals Regs.RegionId join Del in mb.Users on c.Dealer equals Del.UserName where c.Ex == true && c.Country == Country && c.Region == Region && c.City == City select new ClientCi() { City = c.City, Country = c.Country, Dat = c.Dat, Dealer = Del.NameCompany + " (" + Del.KodOPK + ")", KodOPK = c.KodOPK, NameCity = CityP.Name, NameRegion = Regs.Name, NameCompany = c.NameCompany + " (" + c.KodOPK + ")", Note = c.Note, Region = c.Region, UserId = c.UserId }).ToList();

                if (Cl.Count > 0)
                    ViewBag.Clients = Cl;
            }
            return PartialView("Partial1");
        }

        public ActionResult Dealer(int id=0)
        {
            LR();
            List<AdminInfo> a;
            List<AdminInfo> sel = new List<AdminInfo>();
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
                if (id == 1)
                {
                    a = (from u in mb.Users join c in mb.Countries on u.Country equals c.CountryId join r in mb.Regions on u.Region equals r.RegionId join ci in mb.Cities on u.City equals ci.CityId where u.M == true select new AdminInfo() { UserName = u.UserName, KodOPK = u.KodOPK, NameCompany = u.NameCompany, UserId = u.Id, M = u.M, Email = u.Email, ContryName = c.Name, RegionName = r.Name, CityName = ci.Name }).ToList();
                    ViewBag.Title = "Менеджеры";
                    ViewBag.M = true;
                }
                else
                {
                    a = (from u in mb.Users join c in mb.Countries on u.Country equals c.CountryId join r in mb.Regions on u.Region equals r.RegionId join ci in mb.Cities on u.City equals ci.CityId where u.M == false select new AdminInfo() { UserName = u.UserName, KodOPK = u.KodOPK, NameCompany = u.NameCompany, UserId = u.Id, M = u.M, Email = u.Email, ContryName = c.Name, RegionName = r.Name, CityName = ci.Name }).ToList();
                    ViewBag.Title = "Дилеры";
                    ViewBag.M = false;
                }


                foreach (AdminInfo b in a)
                {
                    foreach (string s in UserManager.GetRoles(b.UserId).ToList())
                        if (s == "Dealer")
                        {
                            string ss = "";
                            foreach (RegionsAndDealer r in mb.RegionsAndDealers.Where(x => x.NameUser == b.UserName).ToList())
                            {
                                ss += mb.Regions.Where(x => x.RegionId == r.RegionId).First().Name + "; ";
                            }
                            b.Reg = ss;
                            sel.Add(b);
                            break;
                        }
                }
            }
            ViewBag.Del = sel;
            return View();
        }

        public ActionResult Clients(string id="")
        {
            LR();
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
                List<ClientCi> a =(from c in mb.Clients join CityP in mb.Cities on c.City equals CityP.CityId join Regs in mb.Regions on c.Region equals Regs.RegionId join Del in mb.Users on c.Dealer equals Del.UserName where c.Ex == true && c.Dealer == id select new ClientCi() { City = c.City, Country = c.Country, Dat = c.Dat, Dealer = Del.NameCompany + " (" + Del.KodOPK + ")", KodOPK = c.KodOPK, NameCity = CityP.Name, NameRegion = Regs.Name, NameCompany = c.NameCompany + " (" + c.KodOPK + ")", Note = c.Note, Region = c.Region, UserId = c.UserId }).ToList();
                if (a.Count > 0)
                    ViewBag.Name = a[0].Dealer;
                ViewBag.Clients = a;
                return View();
            }
        }
	}
}