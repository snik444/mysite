﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BetonMashNew.Models;

namespace BetonMashNew.Controllers
{
    public class SystemController : Controller
    {
        //
        // GET: /System/
        public ActionResult GetRegionsSelect(int id = 0)
        {
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
                return PartialView("Partial1", mb.Regions.Where(x => x.Country == id).ToList());
            }
        }

        public ActionResult GetCitiesSelect(int id = 0)
        {
            using (ApplicationDbContext mb = new ApplicationDbContext())
            {
                return PartialView("Partial2", mb.Cities.Where(x => x.Region == id).ToList());
            }
        }

        public ActionResult Error(string Error)
        {
            ViewBag.Er = Error;
            return View("Partial3");

        }
	}
}