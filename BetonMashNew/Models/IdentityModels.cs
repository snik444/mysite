﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Security;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;

namespace BetonMashNew.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        [Required(ErrorMessage = "Поле должно быть установлено")]
        [Display(Name = "Наименование")]
        [StringLength(80, MinimumLength = 1, ErrorMessage = "Длина строки должна быть от 1 до 80 символов")]
        public string NameCompany { get; set; }
        [Required(ErrorMessage = "Поле должно быть установлено")]
        [Display(Name = "Код ОКПО")]
        [StringLength(25, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 25 символов")]
        public string KodOPK { get; set; }
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Неправильный e-mail")]
        public string Email { get; set; }
        public DateTime Dat { get; set; }
        public bool M { get; set; }
        public int? Country { get; set; }
        public int? Region { get; set; }
        public int? City { get; set; }
    }



    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<CountryCl> Countries { get; set; }
        public DbSet<RegionCl> Regions { get; set; }
        public DbSet<CityCl> Cities { get; set; }
        public DbSet<RegionsAndDealer> RegionsAndDealers { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<UpdateClient> UpdateClients { get; set; }

    }

    public class AdminInfo
    {
        public string UserName { get; set; }
        public string UserId { get; set; }
        public string NameCompany { get; set; }
        public string KodOPK { get; set; }
        public string Email { get; set; }
        public string ContryName { get; set; }
        public int ContryId { get; set; }
        public string RegionName { get; set; }
        public int RegionId { get; set; }
        public string CityName { get; set; }
        public int CityId { get; set; }
        public DateTime Dat { get; set; }
        public string Rol { get; set; }
        public string Reg { get; set; }
        public bool M { get; set; }
  //      public List<string> Reg { get; set; }
    }

    public class UsersRegions
    {
        public int IdRegion { get; set; }
        public string NameRegion { get; set; }
    }

    public class MyRegionCl
    {
        public int IdUR { get; set; }
        public int RegionId { get; set; }
        public int Country { get; set; }
        public string Name { get; set; }
    }

    [Table("CountryCl")]
    public class CountryCl
    {
        [Key]
        public int CountryId { get; set; }
        public string Name { get; set; }
    }

    [Table("UpdateClient")]
    public class UpdateClient
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string NameDealer { get; set; }
        public string NameCompany { get; set; }
        public DateTime Dat { get; set; }
        public string Note { get; set; }
    }

    [Table("RegionCl")]
    public class RegionCl
    {
        [Key]
        public int RegionId { get; set; }
        public int Country { get; set; }
        public string Name { get; set; }
    }

    [Table("CityCl")]
    public class CityCl
    {
        [Key]
        public int CityId { get; set; }
        public int Region { get; set; }
        public int Country { get; set; }
        public string Name { get; set; }
    }

    [Table("RegionsAndDealer")]
    public class RegionsAndDealer
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string NameUser { get; set; }
        public int RegionId { get; set; }
 
    }

    [Table("Client")]
    public class Client
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        [Required(ErrorMessage = "Поле должно быть установлено")]
        [Display(Name = "Наименование")]
        [StringLength(80, MinimumLength = 1, ErrorMessage = "Длина строки должна быть от 1 до 80 символов")]
        public string NameCompany { get; set; }
        [Required(ErrorMessage = "Поле должно быть установлено")]
        [Display(Name = "Код ОКПО")]
        [StringLength(25, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 25 символов")]
        public string KodOPK { get; set; }
        public DateTime Dat { get; set; }
        public string Dealer { get; set; }
        [Display(Name = "Примечание, куда может вноситься оборудование отгруженное потребитлю")]
        public string Note { get; set; }
        public int Country { get; set; }
        [Display(Name = "Регион")]
        public int Region { get; set; }
        [Display(Name = "Город")]
        public int City { get; set; }
        public bool Ex { get; set; }
    }

    public class ClientCi
    {
        public int UserId { get; set; }
        public string NameCompany { get; set; }
        public string KodOPK { get; set; }
        public DateTime Dat { get; set; }
        public string Dealer { get; set; }
        public string Note { get; set; }
        public int Country { get; set; }
        public int Region { get; set; }
        public int City { get; set; }
        public string NameRegion { get; set; }
        public string NameCity { get; set; }
    }

}